import paho.mqtt.client as mqtt
import time
# This is the Subscriber

def on_connect(client, userdata, flags, rc):
  print("Connected with result code "+str(rc))
  client.subscribe("topic")
s=""
def on_message(client, userdata, msg):
    s=msg.payload
    print(s)



client = mqtt.Client()

BROCKER_IP="127.0.0.1"
BROKER_PORT=1883

client.connect(BROCKER_IP,BROKER_PORT)

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
