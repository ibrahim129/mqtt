#!/usr/bin/env python3
import time
import paho.mqtt.client as mqtt

# This is the Publisher

client = mqtt.Client()

BROCKER_IP="127.0.0.1"
BROKER_PORT=1883

client.connect(BROCKER_IP,BROKER_PORT)

while 1:
    client.publish("topic", "Hello world!")
    time.sleep(2)
    

client.disconnect();
